import React from 'react';
import BoxWrap from './Box/BoxWrap';
import CardWrap from './Card/CardWrap';

export const Info = () => {
  return (
    <div>
      <BoxWrap />
      <CardWrap />
    </div>
  );
};

export default Info;
