import React from 'react';
import './banner.css';

export const Banner = () => {
  return (
    <div className='banner'>
      <h1 className='banner-head'>
        {'Demo app.'}
      </h1>
    </div>
  );
};

export default Banner;
