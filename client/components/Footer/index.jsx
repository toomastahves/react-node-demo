import React from 'react';
import './footer.css';

export const Footer = () => {
  return (
    <div className='footer l-box'>
      <p>
        {'Demo app by Toomas Tahves'}
        <br />
        {'2016'}
      </p>
    </div>
  );
};

export default Footer;
